Feature: Punk API Testing

Scenario Outline: Get a Single Beer
Given I call the punk api with "beers" id <id>
Then I expect 200 status response
And the "malt" is "Extra Pale"
And the "malt" value is 5.3 and the unit is "kilograms"

Examples:
| id  |
| 192 |