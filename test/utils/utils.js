// This function allows to look for a key within an object.
function lookup(obj, k) {
    if(typeof(obj) != 'object') {
        return null;
    }
    
    var result = null;
    if(obj.hasOwnProperty(k)) {
         return obj[k];
    } else {
      for(var o in obj) {
        if (obj[o] != null){
            result = lookup(obj[o], k)};

        if(result == null) { 
            continue 
        } else { 
            break 
        };
      }
    }
    return result;
}

module.exports = lookup;